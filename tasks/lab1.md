#Lab 1

In this lab you will create simple todo list with the basic concepts of AngularJS which were covered in the first part of the presentation.

#Step 0

Use:

    git checkout step0

in order to view the startup files. To complete step 1 you should:

1. In `index.html`, define new AngularJS application by including the `ng-app` attribute inside the documents body.
2. In `index.html`, include `ng-controller` directive which points that the root controller of the application will be called `TodoCtrl`.
3. Edit `app.js` and include declaration of function called `TodoCtrl`.
4. Define array called `todos` to the scope and add few string values to it.
5. In `index.html`, add unordered list inside the body.
6. In `index.html`, using `ng-repeat` directive iterate through the items of the array and visualize them.


#Step 1

Use:

    git checkout step1

in order to see the solution of the last step. To complete step 2 you should:

1. In `index.html`, add text input and bind it to variable called `currentTodo`.
2. In `index.html`, add button labeled `Add` and execute the `addTodo` method in the expression associated with it.
3. In `app.js`, append `currentTodo` to the array `todos` in method called `addTodo` attached to the scope.


#Step 2

Use:

    git checkout step2

in order to see the solution of the last step. To complete step 3 you should:

1. In `index.html`, append the string `track by $index` inside the `ng-repeat` expression. This will allow duplicates inside the unordered list.
2. In `index.html`, inside the list item template include button with label `x` and on click call `removeTodo` with the index of the item (use `$index`).
3. In `app.js`, add method called `removeTodo` to the scope. It accepts index and removes the item with the specified index from the `todos` array.
