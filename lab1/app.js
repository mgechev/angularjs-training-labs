function TodoCtrl($scope) {
  $scope.todos = ['To write integration tests', 'To deploy'];
  $scope.addTodo = function () {
    $scope.todos.push($scope.currentTodo);
    $scope.currentTodo = undefined;
  };
  $scope.removeTodo = function (index) {
    $scope.todos.splice(index, 1);
  }
}